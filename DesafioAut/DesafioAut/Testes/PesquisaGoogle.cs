﻿using DesafioAut.Pages;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace DesafioAut.Testes
{
    [TestFixture]
    public class PesquisaGoogle : Browser
    {
        public string url = "http://www.google.com";
        Resource resource = new Resource();


        [Test]
        public void Pesquisar()
        {
            AcessarSite(url);

            IWebElement busca = driver.FindElement(By.XPath("//form/div[2]/div[1]/div[1]/div/div[2]/input"));
            busca.SendKeys("iLAB Quality");
            Actions action = new Actions(driver);
            action.SendKeys(Keys.Enter).Build().Perform();

            resource.ValidarResultadoDaPesquisa(driver);
            resource.CapturarImagem("EvidenciaTeste", driver, screenshotsPath);

            driver.Quit();
        }

    }


}
