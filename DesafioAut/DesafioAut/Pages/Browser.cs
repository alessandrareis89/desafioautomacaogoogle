﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace DesafioAut.Pages
{
    public class Browser
    {
        public IWebDriver driver;

        public string screenshotsPath = $"C:\\ScreenshotsAutomacao\\";

        public void AcessarSite(string url)
        {
            driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl(url);
        }

    }
}
