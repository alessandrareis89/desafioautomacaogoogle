﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using DesafioAut.Pages;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;

namespace DesafioAut.Pages
{
    public class Resource : Browser
    {
        public void ValidarResultadoDaPesquisa(IWebDriver driver)
        {
            Assert.IsTrue(driver.FindElement(By.CssSelector("#result-stats")).Displayed);
        }

        public void CapturarImagem(string filename, IWebDriver driver, string screenshotsPath)
        {
            Screenshot(driver, screenshotsPath, filename);
            Thread.Sleep(2000);
        }

        private void Screenshot(IWebDriver driver, string screenshotsPasta, string filename)
        {
            if (!Directory.Exists(screenshotsPasta))
            {
                Directory.CreateDirectory(screenshotsPasta);
            }

            var screenShot = ((ITakesScreenshot)driver).GetScreenshot();
            var nomeArquivo = new StringBuilder(screenshotsPasta + filename);
            screenShot.SaveAsFile(nomeArquivo.ToString(), ScreenshotImageFormat.Jpeg);
        }

    }
}